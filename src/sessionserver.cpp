/****************************************************************************
 **
 ** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
 ** Contact: Qt Software Information (qt-info@nokia.com)
 **
 ** This file is part of the WebClient project on Trolltech Labs.
 **
 ** This file may be used under the terms of the GNU General Public
 ** License version 2.0 or 3.0 as published by the Free Software Foundation
 ** and appearing in the file LICENSE.GPL included in the packaging of
 ** this file.  Please review the following information to ensure GNU
 ** General Public Licensing requirements will be met:
 ** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
 ** http://www.gnu.org/copyleft/gpl.html.
 **
 ** If you are unsure which license is appropriate for your use, please
 ** contact the sales department at qt-sales@nokia.com.
 **
 ** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 ** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 **
 ****************************************************************************/

#include "sessionserver.h"
#include "widgeteventhandler.h"
#include "eventqueue.h"
#include "webclientserver.h"

#include <json.h>

QWidget *sharedRoot = 0;

SessionServer::SessionServer(QWidget *widget, Session *session, Server *server)
{
    if (sharedRoot == 0) {
        sharedRoot = new QWidget();
        sharedRoot->resize(1000, 1000);
        sharedRoot->setAttribute(Qt::WA_DontShowOnScreen);
        sharedRoot->setAttribute(Qt::WA_ForceUpdatesDisabled);
        sharedRoot->setAttribute(Qt::WA_OpaquePaintEvent);
        sharedRoot->show();
    }

    widget->setParent(sharedRoot);
    widget->move(0,0);
    rootWidget = widget;

    widgetEventHandler = new WidgetEventHandler(widget, server);
        
    rootWidget->setAttribute(Qt::WA_DontShowOnScreen);
    rootWidget->setAttribute(Qt::WA_OpaquePaintEvent);

    // try to avoid creating the backingstore
    rootWidget->setAttribute(Qt::WA_PaintOnScreen);
#ifdef QT_WS_X11
    qt_x11_set_global_double_buffer(false);
#endif

    rootWidget->show();
    
    // Make the initial show non-special by processing the show events
    // before enabling the event handler. We want to support refreshing
    // the ui, every request of "/content" should give a complete ui refresh.
    qApp->processEvents();
    qApp->processEvents();
    
    widgetEventHandler->setRootWidget(rootWidget);
    widgetEventHandler->setSession(session);
    
    connect(session, SIGNAL(requestContent(HttpRequest *, HttpResponse *)), 
            this,       SLOT(handleRequest(HttpRequest *, HttpResponse *)));
}

SessionServer::~SessionServer()
{
    delete widgetEventHandler;
    rootWidget->setParent(0);
    delete fakeRoot;
}

void SessionServer::handleRequest(HttpRequest *request, HttpResponse *response)
{
    DEBUG << "SingleUserServer::handleRequest!" << request->path();
    widgetEventHandler->handleRequest(request, response);
}


