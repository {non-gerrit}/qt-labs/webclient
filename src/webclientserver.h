/****************************************************************************
 **
 ** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
 ** Contact: Qt Software Information (qt-info@nokia.com)
 **
 ** This file is part of the WebClient project on Trolltech Labs.
 **
 ** This file may be used under the terms of the GNU General Public
 ** License version 2.0 or 3.0 as published by the Free Software Foundation
 ** and appearing in the file LICENSE.GPL included in the packaging of
 ** this file.  Please review the following information to ensure GNU
 ** General Public Licensing requirements will be met:
 ** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
 ** http://www.gnu.org/copyleft/gpl.html.
 **
 ** If you are unsure which license is appropriate for your use, please
 ** contact the sales department at qt-sales@nokia.com.
 **
 ** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 ** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 **
 ****************************************************************************/

#ifndef CONTENTSERVER_H
#define CONTENTSERVER_H

#include <webclient.h>

#include <QtCore>
#include <QtNetwork>

class HttpRequest
{
public:
    HttpRequest();
    HttpRequest(const QList<QByteArray> &text);
    HttpRequest(QTcpSocket *socket);
    QByteArray path();
    QByteArray cookies();
    QList<QNetworkCookie> parsedCookies();
    QByteArray hostName();
//    QHash<QByteArray, QByteArray> cookieValues();
//private:
    void readText();
    void parseText();
    QByteArray m_path;
    QByteArray m_cookies;
    QList<QNetworkCookie> m_parsedCookies;
    QByteArray m_hostName;
    QList<QByteArray> m_text;
    QTcpSocket *m_socket;
    QByteArray m_ifNoneMatch;
};

class HttpResponse
{
public:
    HttpResponse();
    void setBody(const QByteArray &body);
    void setCookie(const QByteArray &name, const QByteArray &value);
    void setContentType(const QByteArray &contentType);
    void seteTag(const QByteArray &eTag);
    void set304Response();
    void setNeverExpires();

    bool isHandled() const;
    QByteArray toText();

    QByteArray body;
    QByteArray cookie;
    QByteArray contentType;
    QByteArray eTag;
    bool response304;
    bool neverExpires;
};

class Server;
class Session : public QObject
{
Q_OBJECT
public:
    Session(Server *server, int sessionId);
    int sessionId();
    void setIdleSocket(QTcpSocket *socket);
    QTcpSocket * idleSocket();
    void emitRequestContent(HttpRequest *request, HttpResponse *response);
signals:
    void sessionEnd();
    void requestContent(HttpRequest *request, HttpResponse *response);
public slots:
    void contentAvailable();
    void idleSocketDisconnect();
public:
    int m_sessionId;
    QHostAddress address;
    QTcpSocket *m_idleSocket;
    HttpRequest m_idleRequest;
    HttpResponse m_idleResponse;
    Server *m_server;
    QDateTime lastActivityTime;
};

class FileServer : public QObject
{
Q_OBJECT
public:
    FileServer();
public slots:
    virtual void handleRequest(HttpRequest *request, HttpResponse *response);
private:
    QSet<QString> allowedFileNames;
    QHash<QString, QByteArray> fileData;
    QHash<QString, QByteArray> fileDataDeflated;
    QHash<QString, QByteArray> eTags;
};

class Server : public QTcpServer
{
Q_OBJECT
public:
    Server(quint16 port = 1818);
    ~Server();
signals:
//    void authenticate(Session *session);
    void sessionBegin(Session *session);
    void sessionEnd(int sessionId);
public slots:
//    void authenticated(Session *session);
    void contentAvailable(Session *session);
private slots:
    void connectionAvailable();
    void dataOnSocket();
private slots:
    void purgeInactiveSessions();
private:
    void printRequest(const QList<QByteArray> &request);
    QHash<int, Session *> activeSessions;
    QTimer purgeInactiveSessionsTimer; // kills inactive sessions
    int lastSessionVisited;
    int nextCookieId;
    quint16 port;
    FileServer fileServer;

    // statistics
    int bytesRead;
    int dynamicBytesWritten;
    int staticBytesWritten;
    int totalSessions;
    QDateTime serverStart;
    QByteArray createStatiticsPage();

public:
    int activeSessionLimit;
    QByteArray activeSessionLimitHtml;
    int inactiveSessionTimeout;
    bool sendUpdatesForPlainQWidgets;
    bool shouldSkipUpdate(const QByteArray &className);
    QSet<QByteArray> skipUpdatesClasses;

    bool testHint(QWidget *widget, int widgetHint);
    QHash<QWidget *, QSet</*WebClient::WidgetHint*/ int> > widgetHints;
};

#endif
