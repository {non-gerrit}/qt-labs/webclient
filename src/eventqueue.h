/****************************************************************************
 **
 ** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
 ** Contact: Qt Software Information (qt-info@nokia.com)
 **
 ** This file is part of the WebClient project on Trolltech Labs.
 **
 ** This file may be used under the terms of the GNU General Public
 ** License version 2.0 or 3.0 as published by the Free Software Foundation
 ** and appearing in the file LICENSE.GPL included in the packaging of
 ** this file.  Please review the following information to ensure GNU
 ** General Public Licensing requirements will be met:
 ** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
 ** http://www.gnu.org/copyleft/gpl.html.
 **
 ** If you are unsure which license is appropriate for your use, please
 ** contact the sales department at qt-sales@nokia.com.
 **
 ** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 ** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 **
 ****************************************************************************/

#ifndef EVENTQUEUE_H
#define EVENTQUEUE_H

#include <QtGui>
#include <json.h>
#include "webclientserver.h"

struct EventEntry
{
    enum Type { Noop, Update, StaticUpdate, Show, Hide, Geometry, ShowLineEdit, ParentChange, TextUpdate };

    inline EventEntry(int id, Type type)
        :id(id), type(type) {}

    inline EventEntry():id(0), type(Noop) {};
    
    inline bool operator==(const EventEntry &other) const
    {
        return (id == other.id && type == other.type);
    }

    int id;
    Type type;
};

class EventQueue : public QObject
{
public:
    EventQueue(Session *session = 0);
    void setSession(Session *session);
    void reset();
        
    void handleRequest(HttpRequest *request, HttpResponse *response);
    void handleJsonRequest(QByteArray jsonText, HttpResponse *response);
    void handleImageRequest(json_object* jsonRequest, HttpResponse *response);
    void handleStaticImageRequest(json_object* jsonRequest, HttpResponse *response);
    void addUpdateEvent(quintptr id, const QImage &image, QRect updateRect);
    void addStaticUpdateEvent(quintptr id, uint hash, const QByteArray &pngImage);
    void addStaticUpdateEvent(quintptr id);
    void addGeometryEvent(quintptr id, QRect globalGeometry);
    void addParentChangeEvent(quintptr id);
    void addEvent(quintptr id, EventEntry::Type type);
    QByteArray queueToByteArray();
    json_object *toJson(const EventEntry &event) const;
    json_object *jsonShowEvent(const EventEntry &event) const;
    json_object *jsonShowLineEditEvent(const EventEntry &event) const;
    json_object *jsonHideEvent(const EventEntry &event) const;
    json_object *jsonUpdateEvent(const EventEntry &event) const;
    json_object *jsonStaticUpdateEvent(const EventEntry &event) const;
    json_object *jsonGeometryEvent(const EventEntry &event) const;
    json_object *jsonParentChangeEvent(const EventEntry &event) const;
    json_object *jsonTextUpdateEvent(const EventEntry &event) const;
    json_object *toJsonWidgetType(QWidget *widget) const;

    bool isEmpty() { return events.isEmpty(); }
    static QByteArray pngCompress(const QImage &image);
    //private:
    QPointer<Session> m_session;
    QQueue<EventEntry> events;
    QHash<quintptr, QImage> images;
    QHash<quintptr, QByteArray> compressedImages;
    QHash<quintptr, QByteArray> staticCompressedImages; // id -> png data
    QHash<uint, quintptr> staticImageHashToWidgetId; // TODO: figure out a better way to map
    QHash<quintptr, uint> staticWidgetIdToImageHash; // widget id <-> image hash <-> image data

    QHash<quintptr, QRect> geometries;
};

#endif

