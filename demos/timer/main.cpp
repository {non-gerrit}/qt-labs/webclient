#include <QtGui>
#include <webclient.h>

class LabelUpdater : public QWidget
{
Q_OBJECT
public:
    LabelUpdater()
    {
        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(update()));

        label= new QLabel(this);
        label->resize(50, 20);
        label->move(20,20);
        label->setText("0");

        startButton = new QPushButton(this);
        startButton->setText("Start Timer");
        connect(startButton, SIGNAL(clicked()), SLOT(startTimer()));

        counter = 0;
    }

public slots:
    void update()
    {
        label->setText(QString::number(counter++));
    }

    void startTimer()
    {
        timer->start(500);
    }

private:
    int counter;
    QTimer *timer;
    QLabel *label;
    QPushButton * startButton;
};


int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    LabelUpdater lu;

    
/*
    ButtonResponder responder;
    QObject::connect(pushButton, SIGNAL(pressed()), &responder, SLOT(pressed()));
    QObject::connect(pushButton, SIGNAL(released()), &responder, SLOT(released()));
*/
    WebClient server;
    server.setRootWidget(&lu);
    lu.show();

    return app.exec();
}

#include "main.moc"

