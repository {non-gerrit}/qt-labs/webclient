#include <QtGui>
#include <webclient.h>
#include <ui_chat.h>

WebClient *webClient;

class QRcui : public QWidget, public Ui_Form
{
Q_OBJECT
public:
    QRcui();
};

QRcui::QRcui()
{
    setupUi(this);
}


class QRcServer : public QObject
{
Q_OBJECT
public slots:
    void instantiateUi(QWidget **root, Session *session)
    {
        Q_UNUSED(session);

        QRcui *ui = new QRcui();
        webClient->setWidgetHint(ui, WebClient::StaticWidget);

        QMdiArea *mdiArea = new QMdiArea;
        QMdiSubWindow *mdiSub = mdiArea->addSubWindow(ui);
        webClient->setWidgetHint(mdiSub, WebClient::StaticWidget);

        mdiArea->resize(500, 350);
        webClient->setWidgetHint(mdiArea->viewport(), WebClient::StaticWidget);

        *root = mdiArea;
        uis.append(ui);

        // Populate the textedit with the current text.
        QString text;
        foreach (const QString &line, textLines) {
            text += line + "\n";
        }
        ui->textBrowser_chat->setText(text);

        connect(ui->lineEdit_message, SIGNAL(returnPressed()), SLOT(addTextLine()));
        connect(ui->pushButton_send, SIGNAL(clicked()), SLOT(addTextLine()));
        ui->lineEdit_nick->setText(session->address.toString());
    }

    void addTextLine()
    {
        QRcui *ui = reinterpret_cast<QRcui * >(sender()->parent());
        QString message = ui->lineEdit_message->text();
        if (message.isEmpty())
            return;
        textLines.append(ui->lineEdit_nick->text() + ": " + message);
        ui->lineEdit_message->clear();
        while (textLines.count() > 10) {
            textLines.takeFirst();
        }
        updateTextDisplays();
    }

    void updateTextDisplays()
    {
        QString text;

        foreach (const QString &line, textLines) {
            text += line + "\n";
        }

        foreach (QRcui *ui, uis) {
            ui->textBrowser_chat->setText(text);
        }
    }
private:
    QList<QString> textLines;
    QList<QRcui *> uis;
};
int main(int argc, char **argv)
{
    QApplication app(argc, argv);


    webClient = new WebClient;
//    webClient.setActiveSessionLimit(100);

    QRcServer qrcServer;

    QRcServer object;
    QObject::connect(webClient, SIGNAL(newSession(QWidget **, Session *)),
                     &qrcServer, SLOT(instantiateUi(QWidget **, Session *)));

    return app.exec();
}

#include "main.moc"
