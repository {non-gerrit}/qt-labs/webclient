#include <QtGui>
#include <webclient.h>
#include <QWebView>

class ThinBrowser : public QWidget
{
Q_OBJECT
public:
    ThinBrowser()
    {
        const QString startUrl = "http://www.trolltech.com";

        QVBoxLayout *mainLayout = new QVBoxLayout();

        QString text = "<b>Web View example:</b>";
        mainLayout->addWidget(new QLabel(text));

        lineEdit1 = new QLineEdit();
        lineEdit1->setText(startUrl);

        pushButton = new QPushButton();
        pushButton->setText("Go!");
        mainLayout->addWidget(pushButton);
        connect(pushButton, SIGNAL(clicked()), SLOT(loadPage()));

        mainLayout->addWidget(lineEdit1);
        webView = new QWebView();
        mainLayout->addWidget(webView);


        this->setLayout(mainLayout);
   }

public slots:
    void loadPage()
    {
       webView->setUrl(QUrl(lineEdit1->text()));
    }
private:
    QLineEdit *lineEdit1;
    QPushButton *pushButton;
    QWidget *mainWidget;
    QVBoxLayout *mainLayout;
    QWebView *webView;
};

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    ThinBrowser thinBrowser;

    WebClient server;
    server.setRootWidget(&thinBrowser);

    return app.exec();
}

#include "main.moc"

