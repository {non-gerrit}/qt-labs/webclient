#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>

namespace Ui
{
    class MainWindow;
}

class QWebView;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool eventFilter(QObject *obj, QEvent *event);

private slots:
    void loadPages();
    void showPage(int page);
    void pageLoaded();
private:
    Ui::MainWindow *ui;
    int pageCount;
    QList<QWebView *> webViews;
};

#endif // MAINWINDOW_H
