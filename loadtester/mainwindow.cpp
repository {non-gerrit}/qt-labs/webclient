#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QWebView>
#include <QtCore>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    pageCount = 0;
    ui->setupUi(this);
    connect(ui->pushButton_go, SIGNAL(clicked()), SLOT(loadPages()));
    connect(ui->lineEdit_url, SIGNAL(returnPressed()), SLOT(loadPages()));
    connect(ui->spinBox_pageSelect, SIGNAL(valueChanged(int)), SLOT(showPage(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (ui->checkBox_clickThrough->isChecked() == false)
        return true;

    if (event->type() == QEvent::MouseButtonPress
        || event->type() == QEvent::MouseButtonRelease
        || event->type() == QEvent::KeyPress
        || event->type() == QEvent::KeyRelease) {
        qDebug() << "got event";
        foreach (QWebView *webView, webViews) {
            QApplication::sendEvent(obj, event);
        }
        return false;
    }

    return true;
}

void MainWindow::loadPages()
{
    for (int i = 0; i < ui->spinBox_connectCount->value(); ++i) {
        QWebView *webView = new QWebView(this);
        installEventFilter(webView);
        webViews.append(webView);
        webView->load(QUrl(ui->lineEdit_url->text()));
        ui->stackedWidget_pages->addWidget(webView);
    }
    pageCount += ui->spinBox_connectCount->value();
    ui->lineEdit_pageCount->setText(QString::number(pageCount));
    ui->spinBox_pageSelect->setMaximum(pageCount);
}

void MainWindow::showPage(int page)
{
    ui->stackedWidget_pages->setCurrentIndex(page);
}

void MainWindow::pageLoaded()
{

}
